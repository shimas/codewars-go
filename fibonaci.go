/*
Sum Even Fibonacci Numbers

- Write a func named SumEvenFibonacci that takes a parameter of type int and returns a value of type int
- Generate all of the Fibonacci numbers starting with 1 and 2 and ending on the highest number before exceeding the parameter's value

*/
package main
import "fmt"

func SumEvenFibonacci(limit int) int {

  var nrLen, nrAdd, nrEven int

  nr := []int{1, 2}
  
  for {

    nrLen = len(nr)
    nrAdd = nr[nrLen-1] + nr[nrLen-2]

    if nrAdd > limit {
      break;
    }
    
    nr = append(nr, nrAdd)
    
  }
  
  for _, nrArr:= range nr {
      if nrArr%2 == 0 {
        nrEven = nrEven + nrArr
      }   
  }
  
  fmt.Println(nrEven)
  return nrEven
}

func main () {
  SumEvenFibonacci(8)
  SumEvenFibonacci(111111)
  SumEvenFibonacci(8675309)
}
