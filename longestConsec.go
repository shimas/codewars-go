/*
You are given an array strarr of strings and an integer k. 
Your task is to return the first longest string consisting of k consecutive strings taken in the array.

#Example: longest_consec(["zone", "abigail", "theta", "form", "libe", "zas", "theta", "abigail"], 2) --> "abigailtheta"

n being the length of the string array, if n = 0 or k > n or k <= 0 return "".
*/

package main
import (
  "fmt"
  "strings"
)

func LongestConsec(strarr []string, k int) string {
  
  result := ""
  if (len(strarr) == 0) || (k > len(strarr)) || (k <= 0) {
    return ""
  }

  for index := 0; index < len(strarr) - k + 1; index++ {
    s := strings.Join(strarr[index:index+k],"")
      if len(s) > len(result) {
        result = s
      }
  }
  fmt.Println(result)
  return result
}


func main() {
  LongestConsec([]string{"zone", "abigail", "theta", "form", "libe", "zas", "theta", "abigail"}, 2)
  //answer - abigailtheta
}