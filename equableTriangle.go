/*
A triangle is called an equable triangle if its area equals its perimeter. 
Return true, if it is an equable triangle, else return false. You will be provided with the length of sides of the triangle. 
*/

package main
import "math"

func EquableTriangle(a, b, c int) bool {

    s := ( a + b + c ) / 2
    s = s * ( s-a ) * ( s-b ) * ( s-c )
    f := float64(s)
    f = math.Sqrt(f)

    if f == float64(a + b + c) {
      return true
    }else{
      return false
    }

}

func main () {

  EquableTriangle(12,13,5)

}